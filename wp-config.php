<?php
/**
 * Configuración básica de WordPress.
 *
 * Este archivo contiene las siguientes configuraciones: ajustes de MySQL, prefijo de tablas,
 * claves secretas, idioma de WordPress y ABSPATH. Para obtener más información,
 * visita la página del Codex{@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} . Los ajustes de MySQL te los proporcionará tu proveedor de alojamiento web.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** Ajustes de MySQL. Solicita estos datos a tu proveedor de alojamiento web. ** //
/** El nombre de tu base de datos de WordPress */
define( 'DB_NAME', 'hogar' );

/** Tu nombre de usuario de MySQL */
define( 'DB_USER', 'root' );

/** Tu contraseña de MySQL */
define( 'DB_PASSWORD', '' );

/** Host de MySQL (es muy probable que no necesites cambiarlo) */
define( 'DB_HOST', 'localhost' );

/** Codificación de caracteres para la base de datos. */
define( 'DB_CHARSET', 'utf8mb4' );

/** Cotejamiento de la base de datos. No lo modifiques si tienes dudas. */
define('DB_COLLATE', '');

/**#@+
 * Claves únicas de autentificación.
 *
 * Define cada clave secreta con una frase aleatoria distinta.
 * Puedes generarlas usando el {@link https://api.wordpress.org/secret-key/1.1/salt/ servicio de claves secretas de WordPress}
 * Puedes cambiar las claves en cualquier momento para invalidar todas las cookies existentes. Esto forzará a todos los usuarios a volver a hacer login.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY', 'm2!3Dh6eCp<v#W8GcTpO3|VIb~xf>s`|SJT*Q*^`=N|Bl-j^Ae]|R3(VJc!(o)N+' );
define( 'SECURE_AUTH_KEY', '2$r<PA7co{jwSyLwM:KL0KJCar>sD.PJRn&ein:Zx;xf$LDA?$4ADm5[ V[J-cn?' );
define( 'LOGGED_IN_KEY', '2Vr];L9dh$a5eVf-9z~(C>>ln=D2MZ5x}XED81? w(s|rLq&eL>*dO7BGdu3aS`U' );
define( 'NONCE_KEY', 'T2yzVqXUmyg:7rlg]m;{L%Dac$+$eHM{:l0Jvs1x$1u7q,])*RcgU!yRcyVe+$ u' );
define( 'AUTH_SALT', 'T:[^AV8S8b4X)PIEx$Y*L|/XlTJlw#x/Vu>Lb)U#*=U 8G)830oZ>pS/Dl>)J5~$' );
define( 'SECURE_AUTH_SALT', 'oaMO<${+:Ne1fQf1(ze?,w m3,STO~D]A6|5.F^=;mysX*Tb}>hL`40P}%R*,{aN' );
define( 'LOGGED_IN_SALT', '6mK2SIh%HI%`ssC*JT4RKq*Y7,Mt7kh69qAJmy`e#O>6f)A Xw6_WUx=FIpJPzBt' );
define( 'NONCE_SALT', '& 13uR6xP.@ruWJu1!;f04)<Pka.rAkuf{]2ep&eKKB5MG%,P|b4d_ke*t>(s^4T' );

/**#@-*/

/**
 * Prefijo de la base de datos de WordPress.
 *
 * Cambia el prefijo si deseas instalar multiples blogs en una sola base de datos.
 * Emplea solo números, letras y guión bajo.
 */
$table_prefix = 'wp_';


/**
 * Para desarrolladores: modo debug de WordPress.
 *
 * Cambia esto a true para activar la muestra de avisos durante el desarrollo.
 * Se recomienda encarecidamente a los desarrolladores de temas y plugins que usen WP_DEBUG
 * en sus entornos de desarrollo.
 */
define('WP_DEBUG', false);

/* ¡Eso es todo, deja de editar! Feliz blogging */

/** WordPress absolute path to the Wordpress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

